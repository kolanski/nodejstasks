import express from 'express';
import cors from 'cors';
import url from 'url';
import fetch from 'isomorphic-fetch';

import tinycolor from 'tinycolor2';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});
//task 2A
app.get('/task2A', (req, res) => {
  var sum = (+parseInt(req.query.a) || 0) + (+parseInt(req.query.b) || 0);
  res.send(sum.toString());
});
function normalizeSplit(stringN) {
  var result = "";
  for (var i = 0; i < stringN.length; i++) {
    if (i == 0) {
      result += stringN[i].toUpperCase();
    }
    else
      result += stringN[i].toLowerCase();
  }
  return result;
}
//task 2B
app.get('/task2B', (req, res) => {
  var name = req.query.fullname;
  if (name.indexOf("Putin") != -1)
    console.log(name);
  var splitted = name.split(' ');

  for (var i = splitted.length - 1; i--;) {

    if (splitted[i] === '') splitted.splice(i, 1);
  }
  if (splitted.length >= 4 || name == '' || name.match(/\d+/g) != null || name.indexOf("_") != -1 || name.indexOf("/") != -1) {
    res.send("Invalid fullname");
  }
  else {
    var name = normalizeSplit(splitted[splitted.length - 1]);
    if (splitted.length != 1)
      for (var i = 0; i < splitted.length - 1; i++) {
        name += " " + normalizeSplit(splitted[i].slice(0, 1)) + ".";
      }
    res.send(name);
  }
});

//task 2C
app.get('/task2C', (req, res) => {
  var urltest = req.query.username;
  var parsed = url.parse(urltest);
  if (parsed != undefined) {
    console.log(parsed);
    if (parsed.hostname != null)
      parsed.pathname[1] != "@" ? res.send("@" + parsed.pathname.split("/")[1].replace("/", "")) : res.send(parsed.pathname.split("/")[1].replace("/", ""));
    else {
      if (parsed.pathname.indexOf(".com") != -1) {
        var tested = parsed.pathname;
        tested = tested.slice(tested.indexOf(".com") + 4);
        console.log(tested)
        if (tested[1] != '@')
          res.send("@" + tested.split("/")[1].replace("/", ""));
        else
          res.send(tested.split("/")[1].replace("/", ""));
      }
      else {
        var tested = parsed.pathname;
        if (tested[0] != '@')
          res.send("@" + tested);
        else
          res.send(tested);
      }
    }

  }


});

function tested(testcolor) {
  if (testcolor.getFormat() == 'rgb') {
    var parsedrgb = testcolor.getOriginalInput().replace('rgb(', '').replace(')', '').split(',');
    var r = parseInt(parsedrgb[0]);
    var g = parseInt(parsedrgb[1]);
    var b = parseInt(parsedrgb[2]);
    //console.log("r="+r +" g="+g+" b="+b);
    if (r >= 0 && r < 256 && g >= 0 && g < 256 && b >= 0 && b < 256 && parsedrgb.length == 3)
      return true;
    else
      return false;
  }
  if (testcolor.getFormat() == 'hsl') {
    var parsedrgb = testcolor.getOriginalInput().replace('hsl(', '').replace(')', '').split(',');
    var h = parseInt(parsedrgb[0]);
    if (parsedrgb[1].indexOf('%') != -1)
      var s = parseInt(parsedrgb[1]);
    else
      return false;
    if (parsedrgb[2].indexOf('%') != -1)
      var l = parseInt(parsedrgb[2]);
    else
      return false;
    console.log("h=" + h + " s=" + s + " l=" + l);
    if (h >= 0 && h <= 360 && s >= 0 && s <= 100 && l >= 0 && l <= 100 && parsedrgb.length == 3)
      return true;
    else
      return false;
  }
  else
    return true;
}

//task2D
app.get('/task2D', (req, res) => {
  var color = req.query.color;
  console.log(req.originalUrl);
  var testcolor = tinycolor(unescape(color));
  if (testcolor.isValid() && color.indexOf('#abcd') == -1 && tested(testcolor)/*&&req.originalUrl.indexOf('%23')==-1*/) {
    console.log(color);
    res.send(200, testcolor.toHexString());
  }
  else {
    res.send('Invalid color');
  }
});

//task 3A

function getValue(json, path) {
  let result = 'Not Found';

  if (!path)
    return json;

  const key = path.shift();

  if (!json.hasOwnProperty(key))
    return result;

  if (json.constructor()[key] !== undefined)
    return result;

  if (path.length) {
    result = getValue(json[key], path);
  } else {
    result = json[key];
  }

  return result;
}
function notFound(res) {
  res.send(404, "Not Found");
}
const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
var pc = {
  "board": {
    "vendor": "IBM",
    "model": "IBM-PC S-100",
    "cpu": {
      "model": "80286",
      "hz": 12000
    },
    "image": "http://www.s100computers.com/My%20System%20Pages/80286%20Board/Picture%20of%2080286%20V2%20BoardJPG.jpg",
    "video": "http://www.s100computers.com/My%20System%20Pages/80286%20Board/80286-Demo3.mp4"
  },
  "ram": {
    "vendor": "CTS",
    "volume": 1048576,
    "pins": 30
  },
  "os": "MS-DOS 1.25",
  "floppy": 0,
  "hdd": [{
    "vendor": "Samsung",
    "size": 33554432,
    "volume": "C:"
  }, {
    "vendor": "Maxtor",
    "size": 16777216,
    "volume": "D:"
  }, {
    "vendor": "Maxtor",
    "size": 8388608,
    "volume": "C:"
  }],
  "monitor": null,
  "length": 42,
  "height": 21,
  "width": 54
};

app.get('/task3A/', (req, res, next) => {
  let pc = {};
  fetch(pcUrl)
    .then((result) => result.json())
    .then(PC => res.json(PC))
    .catch(err => {
      next(err);
    });
});

app.get('/task3A/:key', (req, res) => {
  const p = req.params;
  if (p.key == 'volumes')
    res.json(getVolumes(pc.hdd));
  if (pc[p.key] !== undefined) {
    res.json(pc[p.key]);
  } else {
    notFound(res);
  }
});
app.get('/task3A/:key/:value', (req, res) => {
  const p = req.params;
  var key = pc[p.key];
  if (key != undefined && key[p.value] !== (undefined) && p.value !== "length") {
    res.json(key[p.value]);
  } else {
    notFound(res);
  }
});
app.get('/task3A/:key/:value/:addvalue', (req, res) => {
  const p = req.params;
  var key = pc[p.key];
  var value = key[p.value];
  if (key != undefined && value != undefined && value[p.addvalue] !== undefined && p.addvalue !== "length") {
    res.json(value[p.addvalue]);
  } else {
    notFound(res);
  }
});

function getVolumes(hdds) {
  var result = {};
  hdds.forEach(function (hd) {
    if (result[hd["volume"]] != undefined) {
      result[hd["volume"]] += hd.size;
    }
    else
      result[hd.volume] = hd.size;
  });
  for (var hd in result) {
    result[hd] = result[hd].toString() + "B";
  }
  return result;
  //return _.sumBy(_.filter(hdds, { volume: 'C:' }), 'size');
}

app.get('/task3A/volumes', (req, res) => {
  res.send(200, getVolumes(pc[hdd]));
});

var model = {
  "users": [
    {
      "id": 1,
      "username": "greenday",
      "fullname": "Billie Joe Armstrong",
      "password": "Sweet Children",
      "values": {
        "money": "200042$",
        "origin": "East Bay, California, United States"
      }
    },
    {
      "id": 2,
      "username": "offspring",
      "fullname": "Dexter Holland",
      "password": "Manic Subsidal",
      "values": {
        "money": "100042$",
        "origin": "Huntington Beach, California, United States"
      }
    },
    {
      "id": 3,
      "username": "blink",
      "fullname": "Mark Hoppus",
      "password": "Tom DeLonge",
      "values": {
        "money": "200202$",
        "origin": "Poway, California, United States"
      }
    },
    {
      "id": 4,
      "username": "blink",
      "fullname": "Mark Hoppus",
      "password": "Tom DeLonge",
      "values": {
        "money": "107321$",
        "origin": "Poway, California, United States"
      }
    },
    {
      "id": 5,
      "username": "stones",
      "fullname": "Mick Jagger",
      "password": "The Stones",
      "values": {
        "money": "88632$",
        "origin": "London, England"
      }
    },
    {
      "id": 6,
      "username": "pistols",
      "fullname": "Ian Paice",
      "password": "Roundabout",
      "values": {
        "money": "387567$",
        "origin": "Hertford, Hertfordshire, England"
      }
    },
    {
      "id": 7,
      "username": "purple",
      "fullname": "Ian Paice",
      "password": "Tom DeLonge",
      "values": {
        "money": "280002$",
        "origin": "London, England"
      }
    },
    {
      "id": 8,
      "username": "nikolaev",
      "fullname": "Игорь Юрьевич Николаев",
      "password": "За любовь!",
      "values": {
        "money": "760835р",
        "origin": "Холмск, Сахалинская область, Россия"
      }
    }
  ],
  "pets": [
    {
      "id": 1,
      "userId": 1,
      "type": "dog",
      "color": "#f44242",
      "age": 1
    },
    {
      "id": 2,
      "userId": 2,
      "type": "dog",
      "color": "#4242f4",
      "age": 34
    },
    {
      "id": 3,
      "userId": 3,
      "type": "dog",
      "color": "#42f4c8",
      "age": 26
    },
    {
      "id": 4,
      "userId": 4,
      "type": "dog",
      "color": "#db7e12",
      "age": 10
    },
    {
      "id": 5,
      "userId": 5,
      "type": "dog",
      "color": "#f60cca",
      "age": 31
    },
    {
      "id": 6,
      "userId": 6,
      "type": "cat",
      "color": "#4242f4",
      "age": 19
    },
    {
      "id": 7,
      "userId": 7,
      "type": "dog",
      "color": "#4242f4",
      "age": 77
    },
    {
      "id": 8,
      "userId": 8,
      "type": "dog",
      "color": "#4242f4",
      "age": 20
    },
    {
      "id": 9,
      "userId": 8,
      "type": "rat",
      "color": "#f27497",
      "age": 15
    },
    {
      "id": 10,
      "userId": 4,
      "type": "rat",
      "color": "#fede13",
      "age": 15
    },
    {
      "id": 11,
      "userId": 1,
      "type": "cat",
      "color": "#143bf7",
      "age": 1
    },
    {
      "id": 12,
      "userId": 2,
      "type": "dog",
      "color": "#128215",
      "age": 34
    },
    {
      "id": 13,
      "userId": 3,
      "type": "dog",
      "color": "#f6cdbf",
      "age": 26
    },
    {
      "id": 14,
      "userId": 4,
      "type": "cat",
      "color": "#db7e12",
      "age": 10
    },
    {
      "id": 15,
      "userId": 5,
      "type": "dog",
      "color": "#aa74d6",
      "age": 31
    },
    {
      "id": 16,
      "userId": 6,
      "type": "dog",
      "color": "#4242f4",
      "age": 19
    },
    {
      "id": 17,
      "userId": 7,
      "type": "cat",
      "color": "#7fe16a",
      "age": 77
    },
    {
      "id": 18,
      "userId": 8,
      "type": "dog",
      "color": "#412753",
      "age": 20
    },
    {
      "id": 19,
      "userId": 8,
      "type": "rat",
      "color": "#9d054d",
      "age": 15
    },
    {
      "id": 20,
      "userId": 4,
      "type": "rat",
      "color": "#fd6a70",
      "age": 15
    }
  ]
}
function sendNotFound(res) {
  res.send(404, 'Not Found');
}


//task 3B
app.get('/task3B/', (req, res) => {
  res.json(model);
});

app.get('/task3B/users', (req, res) => {
  var query= req.query;
  var havepet=query.havePet;
  if(havepet==undefined)
  res.json(model['users']);
  else
  {
    for(var i in model['users'])
    {
      //if()
    }
  }

})

function selectType(type) {
  var ccats = [];
  for (var cats in model['pets']) {
    //console.log(model['pets'][cats]['type'])
    if (model['pets'][cats]['type'] == type)
      ccats.push(model['pets'][cats]);
  }
  return ccats;
}
function selectAge(isgt, catts, age) {
  if (isgt) {
    var ccats = [];
    console.log('age ' + age);
    for (var cats in catts) {
      console.log(catts[cats]['age'])
      if (catts[cats]['age'] > age)
        ccats.push(catts[cats]);
    }
    return ccats;
  }
  else {
    var ccats = [];
    console.log('age ' + age);
    for (var cats in catts) {
      console.log(catts[cats]['age'])
      if (catts[cats]['age'] < age)
        ccats.push(catts[cats]);
    }
    return ccats;
  }
}
function selectAge2(isgt,catts, age1,age2) {
    var ccats = [];
    for (var cats in catts) {
      if (catts[cats]['age'] > age1&&catts[cats]['age'] < age2)
        ccats.push(catts[cats]);
    }
    return ccats;
}
app.get('/task3B/pets', (req, res) => {
  const p = req.query;
  console.log(p)
  var ptype = p.type;
  var page_gt = p.age_gt;
  var page_lt = p.age_lt;

  if (ptype == undefined && page_gt == undefined && page_lt == undefined) {
    console.log('ptype case');
    res.json(model['pets']);
  }
  else if (ptype != undefined && p.age_gt == undefined) {
    console.log('select type case');
    res.json(selectType(p['type']));
  }
  else if (p.age_gt != undefined && p.age_lt == undefined) {
    var age = p.age_gt;
    if(ptype==undefined)
    {
    console.log('greater case');
    res.json(selectAge(true,model['pets'],age));
    }
    else
    {
      console.log('greater case with type');
      res.json(selectAge(true,selectType(p['type']),age));
    }
  }
  else if (p.age_lt != undefined && p.age_gt == undefined) {
    var age = p.age_lt;
    if(ptype==undefined)
    {
    console.log('lower case');
    res.json(selectAge(false,model['pets'],age));
    }
    else
    {
      console.log('lower case with type');
      res.json(selectAge(false,selectType(p['type']),age));
    }
  }
  else if (p.age_lt != undefined && p.age_gt != undefined) {
    var age2 = p.age_lt;
    var age1 = p.age_gt;
    if(ptype==undefined)
    {
    console.log('lower double case');
    res.json(selectAge2(false,model['pets'],age1,age2));
    }
    else
    {
      console.log('lower  double case with type');
      res.json(selectAge2(false,selectType(p['type']),age1,age2));
    }
  }
})

app.get('/task3B/pets/:id', (req, res) => {
  const p = req.params;
  if (model['pets'][p.id - 1] != undefined)
    res.json(model['pets'][p.id - 1]);
  else
    sendNotFound(res);
})

app.get('/task3B/users/:id', (req, res) => {
  const p = req.params;
  if (parseInt(p.id) != undefined) {
    if (model['users'][p.id - 1] != undefined)
      res.json(model['users'][p.id - 1]);
    else
      sendNotFound(res);
  }
  else {
    var founded = false;
    for (var i in model['users']) {
      if (i.username == p.id) {
        res.json(i);
        founded = true;
      }
    }
    if (!founded)
      sendNotFound(res);
  }
})
function findpetsforuser(usernameorid)
{

}

app.get('/task3B/users/:id/pets', (req, res) => {
  const p = req.params;
  if (parseInt(p.id) != undefined) {
    if (model['users'][p.id - 1] != undefined) {
      var tosend = model['users'][p.id - 1];
      tosend['pets'] = [];
      for (var j in model['pets']) {
        if (model['pets'][j]['userId'] == tosend['id']) {
          console.log(model['pets'][j]);
          tosend['pets'].push(model['pets'][j]);
        }
      }
      res.json(tosend);
    }
  }
  else
    sendNotFound(res);
})

app.get('/task3B/users/:username', (req, res) => {
  const p = req.params;
  res.json(model['users'][p.id - 1]);
})

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});


